import { Application } from "pixi.js";
import { useEffect } from "react";

export function PixiPrototype() {
  useEffect(() => {
    const app = new Application();

    const container = document.getElementById("pixi-container");

    if (container) {
      container.appendChild(app.view);
    }

    return () => {
      app.destroy(true, true);
    };
  });

  return <div id="pixi-container"></div>;
}
