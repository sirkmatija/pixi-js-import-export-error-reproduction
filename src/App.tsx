import { PixiPrototype } from "./PixiPrototype";

function App() {
  return <PixiPrototype />;
}

export default App;
